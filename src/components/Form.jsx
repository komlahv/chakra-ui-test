import React from "react";
import {
  FormLabel,
  FormControl,
  Input,
  Button,
} from "@chakra-ui/core";

export default function HookForm() {

  return (
    <form onSubmit=''>
      <FormControl mx='auto' w={{ 'sm': '80vw', 'lg': '50vw' }}>
        <FormLabel htmlFor="firstName">First name</FormLabel>
        <Input name="firstName" placeholder="first name" my='3' />

        <FormLabel htmlFor="lastName">Last name</FormLabel>
        <Input name="lastName" placeholder="last name" my='3' />
        <Button
          mt={4}
          variantColor="teal"
          type="submit"
        >
          Submit
      </Button>
      </FormControl>

    </form>
  );
}