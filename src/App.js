import React from 'react';
import logo from './logo.svg';
import './App.css';
import { theme, ThemeProvider, CSSReset } from "@chakra-ui/core";

import Header from './components/Header';
import MenuDrawer from './components/MenuDrawer';
import Form from './components/Form';

const breakpoints = ["360px", "768px", "1024px", "1440px"];
breakpoints.sm = breakpoints[0];
breakpoints.md = breakpoints[1];
breakpoints.lg = breakpoints[2];
breakpoints.xl = breakpoints[3];

const newTheme = {
  ...theme,
  breakpoints
};

function App() {
  return (
    <ThemeProvider theme={newTheme}>
      <CSSReset />
      <Header />
      <MenuDrawer />
      <Form />
    </ThemeProvider>
  );
}

export default App;
